import os
import sys

# from livis_model.utils.torch_utils import select_device
# from detect_module import *
import settings as s
from pymongo import MongoClient
from bson.objectid import ObjectId

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance


@singleton
class MongoHelper:
    client = None
    def __init__(self):
        if not self.client:
            self.client = MongoClient(host='localhost', port=27017)
        self.db = self.client[s.MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None):
        _DB = s.MONGO_DB
        DB = self.client[_DB]
        if cname in s.MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(s.MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[s.MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]

class opt_config():
    def __init__(self):
        self.base_path = ""
        self.detector_weights_path = "best.pt" 
        self.separate_crop_model = False
        self.classifier_weights = ""
        self.segmentor_weights = ""
        self.ocr_weights = ""
        self.detector_input_image_size = 1280
        self.common_conf_thres = 0.1
        self.iou_thres = 0.2
        self.max_det = 1000
        self.device = ""
        self.line_thickness = 2
        self.hide_labels = False
        self.hide_conf = True
        self.half = False
        self.crop = False
        self.cord = []
        self.crop_class = ""
        self.min_crop_size = None
        self.max_crop_size = None
        self.crop_conf = 0.25
        self.crop_iou = 0.25
        self.padding  = 50
        self.crop_hide_labels = True
        self.crop_hide_conf = True
        self.classes = None
        self.defects = ["rivert-missing","rivet-damage","pad-damage","pad-rivet-missing","pin-damage","pin-missing","wrong-position-assembly"]
        self.feature = ["rivert","pin","pad-rivet","A3","S"]
        self.features_extra = ["bi_barcode","BagID","des_barcode","A3","S"]

        self.visualize = False
        self.individual_thres = {"rivert-missing":0.1,"rivet-damage":0.1,"pad-damage":0.1,"pad-rivet-missing":0.1,"pin-damage":0.1,"pin-missing":0.1,"wrong-position-assembly":0.1}#best_22.pt
        self.rename_labels = {} 
        self.avoid_labels_cords = [{'xmin':0,'ymin':0,'xmax':1280,'ymax':720},{'xmin':0,'ymin':6,'xmax':569,'ymax':548}]
        self.avoid_required_labels = ['person'] # ['person','cell phone']
        self.detector_predictions = None 
       

# opt_config_instance = opt_config()

# opt_config_dict = {
#     "part_type": "FCQT3" ,
#     "base_path": opt_config_instance.base_path,
#     "detector_weights_path": opt_config_instance.detector_weights_path,
#     "detector_input_image_size": opt_config_instance.detector_input_image_size,
#     "common_conf_thres": opt_config_instance.common_conf_thres,
#     "iou_thres": opt_config_instance.iou_thres,
#     "max_det": opt_config_instance.max_det,
#     "device": opt_config_instance.device,
#     "line_thickness": opt_config_instance.line_thickness,
#     "hide_labels": opt_config_instance.hide_labels,
#     "hide_conf": opt_config_instance.hide_conf,
#     "half": opt_config_instance.half,
#     "crop": opt_config_instance.crop,
#     "cord": opt_config_instance.cord,
#     "crop_class": opt_config_instance.crop_class,
#     "min_crop_size": opt_config_instance.min_crop_size,
#     "max_crop_size": opt_config_instance.max_crop_size,
#     "crop_conf": opt_config_instance.crop_conf,
#     "crop_iou": opt_config_instance.crop_iou,
#     "padding": opt_config_instance.padding,
#     "crop_hide_labels": opt_config_instance.crop_hide_labels,
#     "crop_hide_conf": opt_config_instance.crop_hide_conf,
#     "classes": opt_config_instance.classes,
#     "defects": opt_config_instance.defects,
#     "feature": opt_config_instance.feature,
#     "visualize": opt_config_instance.visualize,
#     "individual_thres": opt_config_instance.individual_thres,
# }

# from PIL import Image
# import os
# import glob
# import datetime

# def get_image_info(file_path):
#     with Image.open(file_path) as img:
#         filename = os.path.basename(file_path)
#         image_name = os.path.splitext(os.path.basename(file_path))[0]
#         width, height = img.size
#         mode = img.mode
#         channels = len(img.split())
#         file_size = os.path.getsize(file_path)
#         return filename , image_name , width , height , channels , mode , file_size


# for image_file_path in glob.glob(r'C:\Users\Rakshith\Downloads\input_images\input_images\*.png'):
#     filename , image_name , width , height , channels , mode , file_size = get_image_info(image_file_path)
#     datetime_now = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S") 
#     data = {
#     "file_url": "http://localhost:4500/"+str(filename),
#     "image_name": image_name,
#     "state": None,
#     "annotation_detection": [],
#     "annotation_detection_history": [],
#     "annotation_classification": "",
#     "annotation_classification_history": [],
#     "good": False,
#     "bad": False,
#     "is_flagged": False,
#     "labelled": False,
#     "metadata": {
#         "format": "png",
#         "width": width,
#         "height": height,
#         "space": "srgb",
#         "channels": channels,
#         "depth": "uchar",
#         "density": 72,
#         "chromaSubsampling": "4:2:0",
#         "isProgressive": False,
#         "hasProfile": False,
#         "hasAlpha": False,
#         "size": file_size,
#         "upload_at": datetime_now },
#     "part_id": "61b6eeed003516a5c978328a",
#     "workstation_id": "64d4eab6bc3026ca570f2f48",
#     "workstation_type": "static",
#     "camera_type": "Gigi-cam",
#     "operation_type": "upload",
#     "platform": "Edge+",
#     "isdeleted": False,
#     "created_at": datetime_now
#     }


#     collection = MongoHelper().getCollection('61b6eeed003516a5c978328a_dataset')
#     result = collection.insert_one(data)



# {
#   "_id": {
#     "$oid": "61b6eeed003516a5c978328a"
#   },
#   "part_number": "FQC_LINE_1",
#   "part_description": "",
#   "isdeleted": False,
#   "kanban": {
#     "features": [],
#     "defects": [],
#     "kanban_details": null
#   },
#   "tags": [],
#   "thumbnail_url": "",
#   "preview_url": ""
# }
