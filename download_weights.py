from flask import Flask, request, jsonify
import os
import time
import urllib.request
from pymongo import MongoClient
from bson import ObjectId
from common.utils import MongoHelper , CacheHelper

app = Flask(__name__)

class Deploy:
    def __init__(self):
        self.redis_server = CacheHelper()
    
    def make_dirs(self,path):
        if not os.path.exists(path):
            os.makedirs(path)
            
    def download_weights_url(self,weights_url,part_name,local_filename):
        try:
            start_time = time.time()
            def report_hook(count, block_size, total_size):
                downloaded_bytes = count * block_size
                total_bytes = total_size
                percent_complete = (downloaded_bytes / total_bytes) * 100
                download_speed = downloaded_bytes / (time.time() - start_time) / 1024
                if download_speed > 0:
                    remaining_bytes = total_bytes - downloaded_bytes
                    eta_seconds = remaining_bytes / (download_speed * 1024)
                    self.redis_server.set_json({'estimated_time':f"{eta_seconds:.2f}"})
                    eta_minutes, eta_seconds = divmod(eta_seconds, 60)
                    print(f"Downloaded {downloaded_bytes / 1024:.2f} KB of {total_bytes / 1024:.2f} KB ({percent_complete:.2f}%) - {download_speed:.2f} KB/s - ETA: {int(eta_minutes)}m {int(eta_seconds)}s", end='\r')
                else:
                    print(f"Downloaded {downloaded_bytes / 1024:.2f} KB of {total_bytes / 1024:.2f} KB ({percent_complete:.2f}%) - Download speed: 0 KB/s", end='\r')
                    
            urllib.request.urlretrieve(weights_url, local_filename, reporthook=report_hook)
            return local_filename
        except:
            print(f"Failed to download file. Status code: 500")
            return None

deployment = Deploy()

@app.route('/download_weights', methods=['POST'])
def download_weights():
    try:
        data = request.get_json()
        weights_url = data.get('weights_url')
        part_name = data.get('part_name')
        local_filename = data.get('local_filename')
        if weights_url and part_name:
            local_filename = deployment.download_weights_url(weights_url, part_name ,local_filename)
            if local_filename:
                return jsonify({"message": "Weights downloaded successfully", "local_filename": local_filename}), 200
            else:
                return jsonify({"error": "Failed to download weights"}), 500
        else:
            return jsonify({"error": "Invalid request data"}), 400
    except Exception as e:
        return jsonify({"error": str(e)}), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9600 , debug=True)

