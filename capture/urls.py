from django.urls import path, re_path

from capture import views

urlpatterns = [
    re_path(r'^start_data_acquisition/$', views.start_data_acquisition_view),
    re_path(r'^get_dataset_details/$', views.dataset_metrics_view),
    re_path(r'^get_all_parts/$', views.get_all_part_type_details),
    re_path(r'^set_sync/$', views.set_sync_data_task_view),
    re_path(r'^process_data_transfer/$', views.process_data_transfer_view),
]
