import os
# from kafka import KafkaProducer,  KafkaConsumer
import json
import cv2
import numpy as np
import base64
import time
import sys
import logging
import pandas as pd
from pymongo import MongoClient
from common.utils import MongoHelper, CacheHelper
from django.utils import timezone
from bson import ObjectId
import multiprocessing
from .consumer import *
from livis.settings import *
import uuid
import ast
from PIL import Image
import os
import glob
import datetime
import requests

class DataAcquisition:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        self.datetime_now = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")  
        self.data_capture_collection = self.mongo_server.getCollection('data_capture_details')
        self.parts_collection = self.mongo_server.getCollection('parts')
        self.dataset = self.dataset_metrics()

    def start_data_acquisition(self, data):
        part = data.get('part', None)
        part_type = data.get('part_type', None)
        try:
            if part is not None:
                data_capture_details = self.data_capture_collection.find_one({}, sort=[('_id', -1)])
                if data_capture_details:
                    data_capture_details['data_acquisition'] = 'completed'
                    self.data_capture_collection.update_one({"_id": data_capture_details['_id']}, {"$set": data_capture_details})
                dataset_coll = {} 
                dataset_coll['part'] = part
                dataset_coll['part_type'] = part_type
                dataset_coll['current_captured_image'] = ''
                dataset_coll['images_preview'] = []
                dataset_coll['start_time'] = self.datetime_now
                dataset_coll['produced_on'] = self.datetime_now
                dataset_coll['data_acquisition'] = "started"
                self.data_capture_collection.insert_one(dataset_coll)
                self.redis_server.set_json({'set_plc_data_capture':True})
                print(self.redis_server.get_json('set_plc_data_capture'))
                return 'Data Acquisition is started', 200
            else:
                return 'Please Provide Part Details', 400
        except Exception as e:
            return str(e), 500
        
    def dataset_metrics(self):
        try:
            data_collection_status = False
            data_capture_details = self.data_capture_collection.find_one({"data_acquisition":"started"}, sort=[('_id', -1)])
            if len(data_capture_details['images_preview']) == 14:
                data_collection_status = True  
            dataset_obj = {}
            if data_capture_details:
                dataset_obj['part'] = data_capture_details['part']
                dataset_obj['part_type'] = data_capture_details['part_type']
                dataset_obj['current_captured_image'] = data_capture_details['current_captured_image']
                dataset_obj['images_preview'] = data_capture_details['images_preview']
                dataset_obj['total_image_captures'] = len(data_capture_details['images_preview'])
                dataset_obj['is_capture_started'] = True 
                dataset_obj['is_sync_process'] = self.redis_server.get_json('is_sync_process') 
                dataset_obj['is_data_collection'] = data_collection_status
                data_collection_status = False 
            else :
                data_capture_details = self.data_capture_collection.find_one({"data_acquisition":"completed"}, sort=[('_id', -1)])
                dataset_obj['part'] = data_capture_details['part']
                dataset_obj['is_capture_started'] = False 
                dataset_obj['is_sync_process'] = self.redis_server.get_json('is_sync_process') 
                dataset_obj['is_data_collection'] = data_collection_status
                data_collection_status = False 
            return dataset_obj
        except Exception as e:
            dataset_obj = {}
            dataset_obj['is_capture_started'] = False 
            dataset_obj['is_sync_process'] = self.redis_server.get_json('is_sync_process') 
            dataset_obj['is_data_collection'] = False
            return dataset_obj
    
    def get_all_part_type_details_task(self):
        parts_type_collection_data = [p for p in self.parts_collection.find({'is_deleted': False})]
        print(parts_type_collection_data)
        return parts_type_collection_data

    def set_sync_data_task(self , data):
        part_type = data.get('part_type', None)
        data_capture_details = self.data_capture_collection.find_one({"part_type":part_type,"data_acquisition":"started"}, sort=[('_id', -1)])
        data_capture_details['data_acquisition'] = 'move_data'
        self.data_capture_collection.update_one({"_id": data_capture_details['_id']}, {"$set": data_capture_details})
        try:
            if data_capture_details:
                data_capture_details = self.data_capture_collection.find_one({"part_type":part_type,"data_acquisition":"move_data"}, sort=[('_id', -1)])
                if data_capture_details:
                    self.redis_server.set_json({'is_sync_process':True})
                    data_capture_details['data_acquisition'] = 'completed'
                    dataset_images = data_capture_details['images_preview']
                    self.data_capture_collection.update_one({"_id": data_capture_details['_id']}, {"$set": data_capture_details})
                    url = "http://192.168.32.242:4900/process_data_transfer"
                    data = {"part" : part_type , "dataset_images": dataset_images}
                    json_data = json.dumps(data)
                    headers = {'Content-Type': 'application/json'}
                    response = requests.post(url, data=json_data, headers=headers)
                    time.sleep(0.6)
                    if response.status_code == 200:
                        self.redis_server.set_json({'is_sync_process':False})
                        return 'Data Transfered is completed', 200
                    elif response.status_code == 400:
                        data_capture_details['data_acquisition'] = 'resume'
                        self.data_capture_collection.update_one({"_id": data_capture_details['_id']}, {"$set": data_capture_details})
                        self.redis_server.set_json({'is_sync_process':False})
                        return 'part details has been not added in parts config', 400
                    else:
                        self.redis_server.set_json({'is_sync_process':False})
                        data_capture_details['data_acquisition'] = 'resume'
                        self.data_capture_collection.update_one({"_id": data_capture_details['_id']}, {"$set": data_capture_details})
                        return 'Error while transfering data', 400
                else:
                    return 'Part data is not collected', 400   
            else:
                data_capture_details = self.data_capture_collection.find_one({"part_type":part_type,"data_acquisition":"resume"}, sort=[('_id', -1)])
                if data_capture_details:
                    self.redis_server.set_json({'is_sync_process':True})
                    data_capture_details['data_acquisition'] = 'completed'
                    dataset_images = data_capture_details['images_preview']
                    self.data_capture_collection.update_one({"_id": data_capture_details['_id']}, {"$set": data_capture_details})
                    url = "http://192.168.32.242:4900/process_data_transfer"
                    data = {"part" : part_type , "dataset_images": dataset_images}
                    json_data = json.dumps(data)
                    headers = {'Content-Type': 'application/json'}
                    response = requests.post(url, data=json_data, headers=headers)
                    if response.status_code == 200:
                        self.redis_server.set_json({'is_sync_process':False})
                        return 'Data Transfered is completed', 200
                    elif response.status_code == 400:
                        data_capture_details['data_acquisition'] = 'resume'
                        self.data_capture_collection.update_one({"_id": data_capture_details['_id']}, {"$set": data_capture_details})
                        self.redis_server.set_json({'is_sync_process':False})
                        return 'part details has been not added in parts config', 400
                    else:
                        self.redis_server.set_json({'is_sync_process':False})
                        data_capture_details['data_acquisition'] = 'resume'
                        self.data_capture_collection.update_one({"_id": data_capture_details['_id']}, {"$set": data_capture_details})
                        return 'Error while transfering data', 400
                else:    
                    return 'Part data is not collected', 400   
        except Exception as e:
            return str(e), 500  
    
    
        