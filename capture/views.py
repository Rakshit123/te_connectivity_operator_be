from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import TemplateHTMLRenderer , JSONRenderer
from django.http import HttpResponse
import json
from .utils import *
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from django.http import HttpResponse,StreamingHttpResponse
from accounts.views import check_permission
from common.utils import Encoder


@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def start_data_acquisition_view(request):
    # check_permission(request,"can_capture_image")
    data = json.loads(request.body)
    data_acquisition_instance = DataAcquisition()
    message, status_code = data_acquisition_instance.start_data_acquisition(data)
    if status_code == 200:
        return HttpResponse(json.dumps({'Message': 'Success!', 'data': message ,'start_capture' : True}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({'Message': 'fail!', 'data': message, 'start_capture' : False}), content_type="application/json")

def dataset_metrics_view(request):
    def dataset_stream():
        previous_data = None
        while True:
            data = DataAcquisition()
            if data.dataset != previous_data:
                previous_data = data.dataset
                print(previous_data)
                def json_default(obj):
                    if isinstance(obj, ObjectId):
                        return str(obj)
                    raise TypeError(f"Object of type {obj.__class__.__name__} is not JSON serializable")
                data_json = json.dumps( data.dataset, default=json_default)
                yield f"data: {data_json}\n\n"
    return StreamingHttpResponse(dataset_stream(), content_type="text/event-stream")

@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def get_all_part_type_details(request):
    data_acquisition_instance = DataAcquisition()
    response = data_acquisition_instance.get_all_part_type_details_task()
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")    

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def set_sync_data_task_view(request):
    data = json.loads(request.body)
    data_acquisition_instance = DataAcquisition()
    message, status_code = data_acquisition_instance.set_sync_data_task(data)
    if status_code == 200:
        return HttpResponse(json.dumps({'Message': 'Success!', 'data': message }), content_type="application/json")
    else:
        return HttpResponse(json.dumps({'Message': 'fail!', 'data': message}), content_type="application/json")  
    
@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def process_data_transfer_view(request):
    data = json.loads(request.body)
    data_acquisition_instance = DataAcquisition()
    response,status_code = data_acquisition_instance.process_data_transfer_util(data)
    if status_code != 200:
        return HttpResponse( {response}, status=status_code)
    else:
        return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")