import settings as s
from pymongo import MongoClient
import redis
import pickle
import cv2
import os
import glob
import bson
import time
import datetime

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

@singleton
class MongoHelper:
    client = None
    def __init__(self):
        if not self.client:
            self.client = MongoClient(host=s.MONGO_SERVER_HOST, port=s.MONGO_SERVER_PORT)
        self.db = self.client[s.MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None):
        _DB = s.MONGO_DB
        DB = self.client[_DB]
        if cname in s.MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(s.MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[s.MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]


@singleton
class CacheHelper():
    def __init__(self):
        self.redis_cache = redis.StrictRedis(host=s.REDIS_CLIENT_HOST, port=s.REDIS_CLIENT_PORT, db=0, socket_timeout=1)
        print("REDIS CACHE UP!")

    def get_redis_pipeline(self):
        return self.redis_cache.pipeline()
    
    def set_json(self, dict_obj):
        try:
            k, v = list(dict_obj.items())[0]
            v = pickle.dumps(v)
            return self.redis_cache.set(k, v)
        except redis.ConnectionError:
            return None

    def get_json(self, key):
        try:
            temp = self.redis_cache.get(key)
            if temp:
                temp= pickle.loads(temp)
            return temp
        except redis.ConnectionError:
            return None
        return None

    def execute_pipe_commands(self, commands):
        return None


class Camera:
    def __init__(self, device_name):
        self.device_name = device_name
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        self.data_capture_collection = self.mongo_server.getCollection('data_capture_details')

    def capture_image(self):
        while True:
            print(datetime.datetime.now(), 'Inspection Trigger Trigger ,,,,,,,,,,,,,,,,',self.redis_server.get_json('set_plc_data_capture'),end='\r')
            if self.redis_server.get_json('set_plc_data_capture'):
                input_images = []
                for path in glob.glob('/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/capture/input_images/*.png'):
                    input_image = cv2.imread(path)
                    unique_id = bson.ObjectId()
                    dataset_name = str(unique_id)+'_dataset.png'
                    cv2.imwrite(data_drive_path+dataset_name,input_image)
                    current_image_url = 'http://192.168.32.217:3309/'+dataset_name
                    input_images.append(current_image_url)
                    data_capture_details = self.data_capture_collection.find_one({"data_acquisition":"started"}, sort=[('_id', -1)])
                    if data_capture_details:
                        data_capture_details['current_captured_image'] = current_image_url
                        data_capture_details['images_preview'] = input_images
                        self.data_capture_collection.update_one({"_id": data_capture_details['_id']}, {"$set": data_capture_details})
                    time.sleep(0.1)
                self.redis_server.set_json({'set_plc_data_capture':False})
                data_capture_details_complete = self.data_capture_collection.find_one({"data_acquisition":"started"}, sort=[('_id', -1)])
                
if __name__ == "__main__":
    data_drive_path = '/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/dataset_images/'
    my_camera = Camera("Local Camera")
    my_camera.capture_image()
