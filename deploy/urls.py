from django.urls import path,re_path
from deploy import views


urlpatterns = [
    re_path(r'^get_all_trained_details/$', views.get_all_trained_versions),
    re_path(r'^get_all_deployments/$', views.get_specific_deployment_details),
    re_path(r'^deploy_model/$', views.deploy_model_details),
    re_path(r'^deploy_status/$', views.deploy_model_status),

]
