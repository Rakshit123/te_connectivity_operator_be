from common.utils import MongoHelper , CacheHelper
from bson import ObjectId
from datetime import datetime
import requests
import datetime
import os
import time
import urllib.request
import json

class Deploy:
    def __init__(self):
        self.mongo_server = MongoHelper()
        self.redis_server = CacheHelper()
        self.deployment_collection = self.mongo_server.getCollection('deployment')
        self.config_collection = self.mongo_server.getCollection('config')
        self.parts_collection = self.mongo_server.getCollection('parts')
        self.deploy_status = self.deploy_metrics()
    
    def request_usecase_training_details(self):
        try:
            url = "http://192.168.32.242:7500/livis/v2/data_management/get_usecase_training_details/"
            data_to_send = {"workstation_name":"fqc1"} 
            response = requests.post(url, json=data_to_send)
            if response.status_code == 200:
                data = response.json()
                return data
            else:
                return None
        except Exception as e:
            print(e)
            return None

    def get_version_data(self,input_string):
        last_two_characters = input_string[-5:]
        result = int(last_two_characters)
        return(result)
    
    def get_all_trained_versions(self):
        try:
            data = self.request_usecase_training_details()
            deployment_details = data['data']['deployment']
            for deployment_detail in deployment_details:
                for deployment in deployment_details[deployment_detail]:
                    version = self.get_version_data(deployment['experiment_name'])
                    coll = {
                    "part": deployment_detail,
                    "usecase_id" : deployment['usecase_id'],
                    "experiment_name": deployment['experiment_name'],
                    "weights_url": deployment['base_url'],
                    "version": version,
                    "final_score" : deployment['final_score'],
                    "is_deployed" : False,
                    "created_at" : datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
                    }
                    query = {"part": coll["part"], "usecase_id":coll["usecase_id"], "experiment_name":coll["experiment_name"] ,"version" : coll["version"]}
                    existing_data = self.deployment_collection.find_one(query)
                    if existing_data:
                        pass
                    else:
                        self.deployment_collection.insert_one(coll)
            return "Deployment details Updated" , 200           
        except Exception as e:
            return str(e),500 

    def get_specific_deployment_details(self):
        try:
            list_part = []
            complete_deployment_details = []
            deployment_collection_data =self.deployment_collection.find()
            for deployment_details in deployment_collection_data:
                list_part.append(deployment_details["part"])
            parts_list = list(set(list_part))
            
            for part in parts_list:
                deployment_details = self.deployment_collection.find({"part":part})
                deployment_coll = {
                    "usecase" : part , 
                    "versions" : [],
                    "version_deployed" : None ,
                    "created_at" : None
                }
                for deploymnet in deployment_details:
                    deployment_coll['versions'].append({"version":deploymnet['version'] ,"model_accuracy":deploymnet['final_score'], "is_deployed" : deploymnet["is_deployed"] , "deployment_id":deploymnet["_id"] ,"created_at" : deploymnet["created_at"]})
                    if deploymnet["is_deployed"]:
                        deployment_coll["version_deployed"] = {"version" : deploymnet['version']}
                    deployment_coll['created_at'] = deploymnet['created_at']
                complete_deployment_details.append(deployment_coll)       
            return complete_deployment_details
        except:
            return []
        

    def reset_status(self,deployment_id,deployed_part):
        deployment_collection_data = self.deployment_collection.find()
        for deployment_details in deployment_collection_data:
            if str(deployment_details['_id']) != str(deployment_id) and deployment_details['part'] == deployed_part:
                deployment_details['is_deployed'] = False
                self.deployment_collection.update({'_id': ObjectId(deployment_details['_id'])},{'$set': deployment_details})
    
    def make_dirs(self,path):
        if not os.path.exists(path):
            os.makedirs(path)

    def remove_old_weights(self,local_filename):
        if os.path.exists(local_filename):
            os.remove(local_filename)
            print(f"Deleted existing file: {local_filename}")        
            
    def download_weights(self,weights_url,part_name):
        try:
            weights_path = "load_weights"
            cwd_path = os.getcwd()
            cwd_path = os.path.normpath(cwd_path)  
            cwd_path = cwd_path.replace("\\", "/")
            local_path = os.path.join(cwd_path,weights_path,part_name)
            local_filename = local_path + "/best.pt"
            self.remove_old_weights(local_filename)
            self.make_dirs(local_path)
            url = "http://localhost:9600/download_weights"
            data = {"weights_url" : weights_url , "part_name": part_name , "local_filename": local_filename}
            json_data = json.dumps(data)
            headers = {'Content-Type': 'application/json'}
            response = requests.post(url, data=json_data, headers=headers)
            time.sleep(0.6)
            if response.status_code == 200:
                print(f"Weights Downloaded Succesfully : 200")
                return local_filename
            elif response.status_code == 400:
                print(f"Weights path is not avaialble or main server is not working: 400")
                return None      
            else:
                print(f"Server Errorg: 500")
                return None
        except:
            print(f"Failed to download file. Status code: 500")
            return None

    def deploy_model_details(self,data):
        try:
            deployment_collection_data = self.deployment_collection.find_one({'_id':ObjectId(data.get('deployment_id'))})
            weights_url = deployment_collection_data['weights_url']
            parts_collection_data = self.parts_collection.find({"part":deployment_collection_data['part']})
            self.redis_server.set_json({'is_weight_process':True}) 
            weights_path = self.download_weights(weights_url,deployment_collection_data['part'])
            if weights_path is not None:
                for parts_data in parts_collection_data : 
                    individual_list = parts_data['features'] + parts_data['defects']
                    config_coll = {
                        "part_type":parts_data['part_type'] ,
                        "common_conf_thres": 0.2,
                        "iou_thres": 0.2,
                        "line_thickness": 2,
                        "half": False,
                        "defects" : parts_data['defects'],
                        "feature" : parts_data['features'] ,
                        "individual_thres" : {item: 0.5 for item in individual_list},
                        "weights_path": weights_path,
                        "is_deleted": False,
                    }
                    query = {"part_type": config_coll["part_type"],"defects":parts_data['defects'],"feature":parts_data['features']}
                    existing_data = self.config_collection.find_one(query)
                    delete_data = self.config_collection.find_one({"part_type": config_coll["part_type"]})
                    if existing_data:
                        existing_data['weights_path'] = weights_path
                        self.config_collection.update_one({'_id': existing_data['_id']}, {'$set': existing_data}) 
                    elif delete_data: 
                        delete_data['is_deleted'] = True
                        self.config_collection.update_one({'_id': delete_data['_id']}, {'$set': delete_data})
                        self.config_collection.insert_one(config_coll)   
                    else:
                        self.config_collection.insert_one(config_coll)
                deployment_collection_data['is_deployed'] = True
                self.deployment_collection.update({'_id': ObjectId(data.get('deployment_id'))},{'$set': deployment_collection_data})
                self.reset_status(data.get('deployment_id'),deployment_collection_data['part'])
                self.redis_server.set_json({'is_weight_process':False}) 
                self.redis_server.set_json({'estimated_time':None})
                return "Deployment weights done" , 200           
            else:
                self.redis_server.set_json({'is_weight_process':False}) 
                self.redis_server.set_json({'estimated_time':None})
                return "Deployment was unsuccesfull" ,400  
        except Exception as e:
            self.redis_server.set_json({'is_weight_process':False}) 
            self.redis_server.set_json({'estimated_time':None})
            return str(e),500 

    def deploy_metrics(self):
        try:
            dataset_obj = {}
            dataset_obj['estimated_time'] = self.redis_server.get_json('estimated_time') 
            dataset_obj['is_weight_process'] = self.redis_server.get_json('is_weight_process') 
            return dataset_obj
        except Exception as e:
            dataset_obj = {}
            dataset_obj['estimated_time'] = self.redis_server.get_json('estimated_time') 
            dataset_obj['is_weight_process'] = self.redis_server.get_json('is_weight_process')
            return dataset_obj
