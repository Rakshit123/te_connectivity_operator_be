from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import TemplateHTMLRenderer,JSONRenderer
from django.http import HttpResponse
import json
from common.utils import *
from common.utils import Encoder
from drf_yasg import openapi
from drf_yasg.openapi import Schema, TYPE_OBJECT, TYPE_STRING, TYPE_ARRAY
from drf_yasg.utils import swagger_auto_schema
from logs.utils import add_logs_util
from accounts.views import check_permission
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from deploy.utils import Deploy
from django.http import HttpResponse,StreamingHttpResponse

deploy_util = Deploy()

@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        'part_number': openapi.Schema(type=openapi.TYPE_STRING, example='pt11'),
        'part_description': openapi.Schema(type=openapi.TYPE_STRING, example='fjjff')
    }
))


@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def deploy_model_details(request):
    data = json.loads(request.body)
    response = deploy_util.deploy_model_details(data)
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")

@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def get_specific_deployment_details(request):
    response = deploy_util.get_specific_deployment_details()
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")

@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def get_all_trained_versions(request):
    response,status = deploy_util.get_all_trained_versions()
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")

def deploy_model_status(request):
    def deploy_stream():
        previous_data = None
        while True:
            deploy = Deploy()
            if deploy.deploy_status != previous_data:
                previous_data = deploy.deploy_status
                def json_default(obj):
                    if isinstance(obj, ObjectId):
                        return str(obj)
                    raise TypeError(f"Object of type {obj.__class__.__name__} is not JSON serializable")
                data_json = json.dumps( deploy.deploy_status, default=json_default)
                yield f"data: {data_json}\n\n"
    return StreamingHttpResponse(deploy_stream(), content_type="text/event-stream")

