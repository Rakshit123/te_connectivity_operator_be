from ast import operator
from common.utils import *
from bson import ObjectId
from livis.settings import *
from django.utils import timezone
from pymongo import MongoClient
from csv import DictWriter
from datetime import date, datetime


class MegaReportUtil:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        
    def sort_time(slef,val):
        return sorted(val,key=lambda k: k['time_stamp'])

    def get_query_megareport(self ,data):
        from_date = data.get('from_date', None)
        to_date = data.get('to_date', None)
        part_id = data.get('part_id', None)  
        part = data.get('part_name', None)
        work_order = data.get('work_order', None)
        query = []

        if from_date:
            query.append({'start_time': {"$gte": from_date}})
        if to_date:
            query.append({'end_time': {"$lte": to_date}})
        if part :
            query.append({'part_type': part})
        if work_order:
            query.append({'work_order': work_order})
        if part_id:
            query.append({'work_order': part_id})
        return query
    
    def get_megareport(self, data):
        try:
            skip = None
            limit = None
            try:
                skip = int(data.get('skip', None))
                limit = int(data.get('limit', None))
            except:
                skip = 0
                limit = 10
            status = data.get('status', None)  
            query = self.get_query_megareport(data)
            if bool(query):
                inspectionid_collection = self.mongo_server.getCollection('inspection')
                objs = [i for i in inspectionid_collection.find({"$and": query}).sort([('$natural', -1)])]
            else:
                inspectionid_collection = self.mongo_server.getCollection('inspection')
                objs = [i for i in inspectionid_collection.find().sort([('$natural', -1)])]
            print(objs)

            p = []
            pr = []
            total_length = 0

            for ins in objs:
                # model_selected = ins['part_type']
                inspection_id = str(ins['_id'])
                log_coll = self.mongo_server.getCollection(str(inspection_id) + "_results")
                if status:
                    pr = [i for i in log_coll.find({'complete_inspection_details.status': status}).sort([('time_stamp', -1)])]
                    total_length += len(pr)
                else:
                    pr = [i for i in log_coll.find().sort([('time_stamp', -1)])]
                    total_length += len(pr)
                p.extend(pr)
            q = []
            if skip is not None and limit is not None:
                for items in p[skip:skip + limit]:
                    q.append(items)
            else:
                q = p.copy()
            response = {"data": q, "total_length": total_length}
            return response  , 200 
        except Exception as response:
            return str(response)  , 500 

    def get_current_details_utll(self, data):
        try:
            skip = None
            limit = None
            try:
                skip = int(data.get('skip', None))
                limit = int(data.get('limit', None))
            except:
                skip = 0
                limit = 10
            status = data.get('status', None)  
            current_inspection_details = self.mongo_server.getCollection('current_inspection_details')
            current_inspection_doc = current_inspection_details.find_one()
            current_inspection_id = current_inspection_doc['current_inspection_id']
            p = []
            pr = []
            total_length = 0
            inspection_id = str(current_inspection_id)
            log_coll = self.mongo_server.getCollection(str(inspection_id) + "_results")
            if status:
                pr = [i for i in log_coll.find({'complete_inspection_details.status': status}).sort([('time_stamp', -1)])]
                total_length += len(pr)
            else:
                pr = [i for i in log_coll.find().sort([('time_stamp', -1)])]
                total_length += len(pr)
            p.extend(pr)
            q = []
            if skip is not None and limit is not None:
                for items in p[skip:skip + limit]:
                    q.append(items)
            else:
                q = p.copy()
            response = {"data": q, "total_length": total_length}
            return response  , 200 
        except Exception as response:
            return str(response)  , 500 

    def set_flag_util(self,data):
        obj_id = data['inspection_id']
        variables = ['message', 'is_flag']
        coll = {var: data.get(var, None) for var in variables}
        inspection = self.mongo_server.getCollection('inspection')
        for doc in inspection.find().sort("_id", -1):
            mp =  self.mongo_server.getCollection(str(doc['_id']) + "_results")
            mp.update_one({'_id': ObjectId(str(obj_id))}, {'$set': coll})
        return "success" , 200

    def edit_remark_util(self,data):
        master_obj_id = data['master_obj_id']
        slave_obj_id = data['slave_obj_id']
        remark = data['remarks']
        mp = self.mongo_server.getCollection(master_obj_id)
        process_attributes = mp.find_one({'_id' : ObjectId(slave_obj_id)})
        process_attributes['remarks']= remark
        mp.update({'_id' : ObjectId(slave_obj_id) }, {'$set' :  process_attributes})
        process_attributes = mp.find_one({'_id' : ObjectId(slave_obj_id)})
        return "success"

    def dict_to_list(dicti):
        return list(dicti.keys())

    def get_query_export(self ,data):
        from_date = data.get('from_date', None)
        to_date = data.get('to_date', None)
        status = data.get('status', None)
        part_name = data.get('part_name', None)

        query = []
        if from_date:
            query.append({'start_time': {"$gte": from_date}})
        if to_date:
            query.append({'end_time': {"$lte": to_date}})
        if part_name:
            query.append({'part_name': part_name})
        return query

    def export_csv(self ,data):
        query = self.get_query_export(data)
        status = data.get('status', None)
        if bool(query):
            inspectionid_collection = self.mongo_server.getCollection('inspection')
            objs = [i for i in inspectionid_collection.find({"$and": query}).sort([('$natural', -1)])]
        else:
            inspectionid_collection = self.mongo_server.getCollection('inspection')
            objs = [i for i in inspectionid_collection.find().sort([('$natural', -1)])]

        p = []
        total_length = 0

        for ins in objs:
            inspection_id = str(ins['_id'])
            log_coll = self.mongo_server.getCollection(str(inspection_id) + "_results")

            pr = []
            for i in log_coll.find({'status': status}) if status else log_coll.find():
                i['WORK_ORDER-ID'] = i['work_order']
                i['PART_ID'] = i['part_id']
                i['PART'] = i['part']
                i['INSPECTION_RESULTS'] = i ['complete_inspection_details']
                i['CREADTED_AT'] = i['created_at']
                i['TIME_STAMP'] = i['time_stamp']
                if 'message' in i.keys():  
                    i['FLAG_MESSAGE'] = i['message']
                    i['IS_INSPECTION_FLAGGED'] = i['is_flag']
                if 'operator' in i.keys():
                    i['OPERATOR_NAME'] = i['operator']    
                else:
                    i['OPERATOR_NAME'] = None        
                pr.append(i)

            total_length += len(pr)   
            p.extend(pr)

        for items in p:
            keys_to_remove = [
                'save_inspection_image_path', '_id', 'inspection_stage_one', 'inspection_stage_two', 'input_images', 'check_box_details',
                'isFlagged','work_order','part_id','part','datadrive','complete_inspection_details','message','is_flag','created_at','time_stamp','operator',
            ]
            for key in keys_to_remove:
                if key in items.keys():
                    del items[key]

        bsf = str(ObjectId())
        file_name = "/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/datadrive/" + bsf
        list_dict = p

        if list_dict:
            ordered_list = list(list_dict[0].keys())
            with open(file_name + '.csv', 'w') as outfile:
                writer = DictWriter(outfile , fieldnames=['OPERATOR_NAME','WORK_ORDER-ID','PART_ID','PART','INSPECTION_RESULTS','FLAG_MESSAGE', 'IS_INSPECTION_FLAGGED','CREADTED_AT','TIME_STAMP'])
                writer.writeheader()
                writer.writerows(list_dict)

        fn = file_name + '.csv'
        fn = fn.replace("/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/datadrive/", "http://localhost:3306")
        file_w = fn
        return "http://localhost:3306/" + bsf + ".csv"



