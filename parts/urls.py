from django.urls import path,re_path
from parts import views


urlpatterns = [
    re_path(r'^add_part/$', views.add_part_details),
    re_path(r'^delete_part/$', views.delete_part),
    re_path(r'^update_part/$', views.update_part),
    re_path(r'^get_all_parts/$', views.get_all_part_details),
    re_path(r'^get_all_parts_type/$', views.get_all_part_type_details),
    re_path(r'^get_all_color_details/$', views.get_all_color_details),
    re_path(r'^update_parts_details/$', views.update_parts_details),

]
