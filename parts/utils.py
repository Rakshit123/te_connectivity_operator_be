from common.utils import MongoHelper
from bson import ObjectId
from datetime import datetime
import requests

class Part:
    def __init__(self):
        self.mongo_server = MongoHelper()
        self.parts_collection = self.mongo_server.getCollection('parts')
        self.config_collection = self.mongo_server.getCollection('config')

    def add_part_details_task(self, data):
        """
        {
        "part_number": "pt11",
        "part_description": "fjjff"
        }
        """
        now = datetime.now()
        try:
            part = data.get('part', None)
            part_type = data.get('part_type', None)
            current_user = data.get('current_user', None)
            created_by = current_user['first_name']
            modified_by = current_user['first_name']
            created_by_time = now.strftime("%d/%m/%Y %H:%M")
            modified_by_time = now.strftime("%d/%m/%Y %H:%M")

            collection_obj = {
                "part": part,
                "part_type": part_type,
                'created_by': created_by,
                "modified_by": modified_by,
                "created_time": created_by_time,
                "modified_time": modified_by_time,
                "isdeleted": False,
            }
            part_id = self.parts_collection.insert_one(collection_obj)
            return "Succesfully added part: " , 200
        except Exception as e:
            return "Could not add part: " + str(e) , 500

    def delete_part_task(self, data):
        _id = data.get('part_name', None)
        p = self.parts_collection.find_one({'_id': ObjectId(_id)})
        if p:
            isdeleted = p.get('isdeleted')
            if not isdeleted:
                p['isdeleted'] = True
                self.parts_collection.update({'_id': p['_id']}, {'$set': p})
            return _id
        else:
            return "Part not found."

    def update_part_task(self, data):
        """
        {
            "_id": "242798143hdw7q33913413we2",
            "part_number": "pt11",
            "part_description": "fjjff"
        }
        """
        _id = data.get('_id')
        now = datetime.now()

        if _id:
            pc = self.parts_collection.find_one({'_id': ObjectId(_id)})
            if pc:
                part_name = data.get('edit_part_name', None)
                part_type = data.get('edit_part_type', None)
                features = data.get('edit_defects', None)
                defects = data.get('edit_features', None)
                kanban = data.get('kanban', None)
                current_user = data.get('current_user', None)
                modified_by = current_user['first_name']
                modified_by_time = now.strftime("%d/%m/%Y %H:%M")
                if part_name:
                    pc['select_model'] = part_name
                if part_type:
                    pc['label_type'] = part_type
                if part_type:
                    pc['modified_by'] = modified_by
                if part_type:
                    pc['modified_by_time'] = modified_by_time
                self.parts_collection.update({'_id': pc['_id']}, {'$set': pc})
            else:
                return "Part not found"
            return "Updated Successfully"
        else:
            return "Please enter the part ID."
        
    def remove_comman(self , parts_collection_data):
        unique_parts = set()
        unique_data = []
        for item in parts_collection_data:
            part = item["part"]
            if part not in unique_parts:
                unique_parts.add(part)
                unique_data.append(item)
        return unique_data

    def get_all_part_details_task(self):
        parts_collection_data = [p for p in self.parts_collection.find({'is_deleted': False})]
        if parts_collection_data:
            for part_details in parts_collection_data :
                part_details.pop("part_type")
        parts_collection_data = self.remove_comman(parts_collection_data)       
        return parts_collection_data
     
    def get_all_part_type_details_task(self,data):
        part = data['part']
        parts_type_collection_data = [p for p in self.parts_collection.find({'part': part ,'is_deleted': False})]
        print(parts_type_collection_data)
        return parts_type_collection_data
    
    def get_all_color_details_task(self,data):
        part_type = data['part_type']
        parts_type_collection_data = [p for p in self.parts_collection.find({'part_type': part_type ,'is_deleted': False})]
        print(parts_type_collection_data)
        return parts_type_collection_data
        
    def request_usecase_details(self):
        try:
            url = "http://192.168.32.242:7500/livis/v2/data_management/get_usecase_parts_details/"
            data_to_send = {"workstation_name":"fqc1"} 
            response = requests.post(url, json=data_to_send)
            if response.status_code == 200:
                data = response.json()
                return data
            else:
                return None
        except Exception as e:
            print(e)
            return None
 
    def update_parts_details(self):
        try:
            self.parts_collection.remove({})
            data = self.request_usecase_details()
            if data is not None:
                part_details = data['data']['deployment']
                for part_detail in part_details:
                    for part in part_details[part_detail]:
                        coll = {
                        "part_id" : part['_id'],
                        "part": part_detail,
                        "part_type": part['part_name'],
                        "features" : part['overall_features'],
                        "defects" : part['overall_defects'],
                        "is_deleted": False,
                        }
                        self.parts_collection.insert_one(coll)
                return "Parts details updated Successfully",200   
            else:
                return "Error in the Main Server",500 
        except Exception as e:
            return str(e),500    


     
