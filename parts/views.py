from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import TemplateHTMLRenderer,JSONRenderer
from django.http import HttpResponse
from parts.utils import *
import json
from common.utils import *
from common.utils import Encoder
from drf_yasg import openapi
from drf_yasg.openapi import Schema, TYPE_OBJECT, TYPE_STRING, TYPE_ARRAY
from drf_yasg.utils import swagger_auto_schema
from logs.utils import add_logs_util
from accounts.views import check_permission
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from parts.utils import Part


@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        'part_number': openapi.Schema(type=openapi.TYPE_STRING, example='pt11'),
        'part_description': openapi.Schema(type=openapi.TYPE_STRING, example='fjjff')
    }
))

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
def add_part_details(request):
    data = json.loads(request.body)
    part_util = Part()
    part_id = part_util.add_part_details_task(data)
    return HttpResponse(json.dumps({'message' : 'Part added Successfully!'}, cls=Encoder), content_type="application/json")

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
def delete_part(request):
    check_permission(request,"can_delete_part")
    data = json.loads(request.body)
    part_util = Part()
    part_util.delete_part_task(data)
    return HttpResponse(json.dumps({'message' : 'Part deleted Successfully!'}, cls=Encoder), content_type="application/json")


@swagger_auto_schema(method='patch', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        '_id' : openapi.Schema(type=openapi.TYPE_STRING, example='5f32677047b362fbb536f1c0'),
        'part_number': openapi.Schema(type=openapi.TYPE_STRING, example='pt11'),
        'part_description': openapi.Schema(type=openapi.TYPE_STRING, example='fjjff')
    }
))

@api_view(['PATCH'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
def update_part(request):
    check_permission(request,"can_update_part")
    data = json.loads(request.body)
    part_util = Part()
    response = part_util.update_part_task(data)
    return HttpResponse(json.dumps({'message' : response}, cls=Encoder), content_type="application/json")

@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def get_all_part_details(request):
    part_util = Part()
    response = part_util.get_all_part_details_task()
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def get_all_part_type_details(request):
    data = json.loads(request.body)
    part_util = Part()
    response = part_util.get_all_part_type_details_task(data)
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def get_all_color_details(request):
    data = json.loads(request.body)
    part_util = Part()
    response = part_util.get_all_color_details_task(data)
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")

@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def update_parts_details(request):
    part_util = Part()
    response,status = part_util.update_parts_details()
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")