from django.urls import path,re_path
from config import views


urlpatterns = [
    re_path(r'^add_config/$', views.add_config),
    re_path(r'^delete_config/$', views.delete_config),
    re_path(r'^update_config/$', views.update_config),
    re_path(r'^get_all_config/$', views.get_all_config),
    re_path(r'^get_specific_config/$', views.get_specific_config),
]
