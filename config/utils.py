from common.utils import MongoHelper
from bson import ObjectId
from datetime import datetime

class Config:
    def __init__(self):
        self.mongo_server = MongoHelper()
        self.config_collection = self.mongo_server.getCollection('config')

    def add_config_details_task(self, data):
        try:
            part_type = data.get('part_type', None)
            common_conf_thres = data.get('common_conf_thres', None)
            iou_thres = data.get('iou_thres', None)
            input_image_size = data.get('detector_input_image_size', None)
            individual_thres = data.get('individual_thres', None)
            config_dict = {
                "part_type": part_type ,
                "detector_weights_path": "",
                "detector_input_image_size": input_image_size ,
                "common_conf_thres": common_conf_thres,
                "iou_thres": iou_thres,
                "max_det": 1000,
                "device": 'cuda:0',
                "line_thickness": 2,
                "hide_labels": False,
                "hide_conf": True,
                "half": False,
                "crop": False,
                "cord": [],
                "crop_conf": 0.25,
                "crop_iou": 0.25,
                "padding": 50,
                "crop_hide_labels": True,
                "crop_hide_conf": True,
                "classes": None,
                "defects": [],
                "feature": [],
                "visualize": False,
                "individual_thres": individual_thres,
                "is_deleted": False,
            }
            part_id = self.config_collection.insert_one(config_dict)
            return "Succesfully created part config" , 200
        except Exception as e:
            return "Could'nt created part config" + str(e) , 500

    def delete_config_task(self, data):
        config_id = data.get('config_id', None)
        config_details = self.config_collection.find_one({'_id': ObjectId(config_id)})
        if config_details:
            is_deleted = config_details.get('is_deleted')
            if is_deleted:
                config_details['is_deleted'] = True
                self.config_collection.update({'_id': config_details['_id']}, {'$set': config_details})
            return "Succesfully deleted part config" , 200
        else:
            return "Unable to fetch part config details" , 500

    def update_config_task(self, data):
        config_id = data.get('config_id', None)
        if config_id:
            config_details = self.config_collection.find_one({'_id': ObjectId(config_id)})
            if config_details:
                common_conf_thres = data.get('common_conf_thres', None)
                individual_thres = data.get('individual_thres', None)
                if common_conf_thres:
                    config_details['common_conf_thres'] = common_conf_thres
                if individual_thres:
                    config_details['individual_thres'] = individual_thres
                self.config_collection.update({'_id': config_details['_id']}, {'$set': config_details})
                return "Succesfully updated part config" , 200
            else:
                return "Unable to fetch part config details" , 500
        else:
            return "Please enter the Config Part ID details." , 500
    
    def get_all_config_task(self):
        config_collection_data = [p for p in self.config_collection.find({'is_deleted': False})]
        config_list = [] 
        if config_collection_data:
            for config_details in config_collection_data:
                config = {}  
                config["config_id"] = str(config_details["_id"])
                config["part_type"] = str(config_details["part_type"])
                config_list.append(config)
        return config_list
    
    def get_specific_config_task(self,data):
        config_collection_data = self.config_collection.find_one({'part_type': data.get('part_type'),'is_deleted': False})
        if config_collection_data:
            config_collection_data['config_id'] = config_collection_data['_id']
            return config_collection_data
        else:
            return {}
      