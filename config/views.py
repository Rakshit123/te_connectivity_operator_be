from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import TemplateHTMLRenderer,JSONRenderer
from django.http import HttpResponse
import json
from common.utils import *
from common.utils import Encoder
from drf_yasg import openapi
from drf_yasg.openapi import Schema, TYPE_OBJECT, TYPE_STRING, TYPE_ARRAY
from drf_yasg.utils import swagger_auto_schema
from logs.utils import add_logs_util
from accounts.views import check_permission
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from config.utils import Config

@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        'part_number': openapi.Schema(type=openapi.TYPE_STRING, example='pt11'),
        'part_description': openapi.Schema(type=openapi.TYPE_STRING, example='fjjff')
    }
))

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
def add_config(request):
    data = json.loads(request.body)
    config = Config()
    part_id = config.add_config_details_task(data)
    return HttpResponse(json.dumps({'message' : 'Config added Successfully!'}, cls=Encoder), content_type="application/json")

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
def delete_config(request):
    check_permission(request,"can_delete_part")
    data = json.loads(request.body)
    config = Config()
    config.delete_config_task(data)
    return HttpResponse(json.dumps({'message' : 'Config deleted Successfully!'}, cls=Encoder), content_type="application/json")


# @swagger_auto_schema(method='patch', request_body=openapi.Schema(
#     type=openapi.TYPE_OBJECT, 
#     properties={
#         '_id' : openapi.Schema(type=openapi.TYPE_STRING, example='5f32677047b362fbb536f1c0'),
#         'part_number': openapi.Schema(type=openapi.TYPE_STRING, example='pt11'),
#         'part_description': openapi.Schema(type=openapi.TYPE_STRING, example='fjjff')
#     }
# ))

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def update_config(request):
    data = json.loads(request.body)
    config = Config()
    response = config.update_config_task(data)
    return HttpResponse(json.dumps({'message' : 'Config Updated Successfully!'}, cls=Encoder), content_type="application/json")

@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
def get_part_details(request, part_id):
    from parts.utils import get_part_details_task
    response = get_part_details_task(part_id)
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")

@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def get_all_config(request):
    config = Config()
    response = config.get_all_config_task()
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def get_specific_config(request):
    config = Config()
    data = json.loads(request.body)
    response = config.get_specific_config_task(data)
    return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")



