import settings as s
from pymongo import MongoClient
import redis
import pickle
import cv2
import os
import glob

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

@singleton
class MongoHelper:
    client = None
    def __init__(self):
        if not self.client:
            self.client = MongoClient(host=s.MONGO_SERVER_HOST, port=s.MONGO_SERVER_PORT)
        self.db = self.client[s.MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None):
        _DB = s.MONGO_DB
        DB = self.client[_DB]
        if cname in s.MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(s.MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[s.MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]


@singleton
class CacheHelper():
    def __init__(self):
        self.redis_cache = redis.StrictRedis(host=s.REDIS_CLIENT_HOST, port=s.REDIS_CLIENT_PORT, db=0, socket_timeout=1)
        print("REDIS CACHE UP!")

    def get_redis_pipeline(self):
        return self.redis_cache.pipeline()
    
    def set_json(self, dict_obj):
        try:
            k, v = list(dict_obj.items())[0]
            v = pickle.dumps(v)
            return self.redis_cache.set(k, v)
        except redis.ConnectionError:
            return None

    def get_json(self, key):
        try:
            temp = self.redis_cache.get(key)
            if temp:
                temp= pickle.loads(temp)
            return temp
        except redis.ConnectionError:
            return None
        return None

    def execute_pipe_commands(self, commands):
        return None

class Config:
    def __init__(self, dictionary):
        self.__dict__['_dict'] = dictionary

    def __getattr__(self, key):
        if key in self._dict:
            return self._dict[key]
        else:
            raise AttributeError(f"'DotDict' object has no attribute '{key}'")

    def __setattr__(self, key, value):
        self._dict[key] = value

    def __str__(self):
        return str(self._dict)


class Process:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        
    def check_dirs(self,path):
        if not os.path.isdir(path):
            os.makedirs(path) 
            
    def frames_sequence(self,input_folder_path):
        file_names = {
            'top': None,
            'bottom': None,
            'side1': None,
            'side2': None,
            'side3': None,
            'side4': None,
            'side5': None,
            'side6': None
        }

        for file_path in glob.glob(os.path.join(input_folder_path, '*.png')):
            file_name = os.path.basename(file_path)
            for key in file_names.keys():
                if file_name.endswith(key + '.png'):
                    file_names[key] = file_path

        frames_list = [file_names[key] for key in ['top', 'side1', 'side2', 'side3', 'side4', 'side5', 'side6', 'bottom']]
        return frames_list
    
    def get_only_list(self, defect_nest_list):
        return [d for defect in defect_nest_list for d in defect]

    def get_defect_list(self, detector_predictions):
        parameters = self.mongo_server.getCollection("paramaters")
        family = self.mongo_server.getCollection("part").find_one()['family']    
        return [i for x in parameters.find() if x['family'] == family for i in x.get('defects') if i in detector_predictions]

    def get_feature_list(self, detector_predictions):
        parameters = self.mongo_server.getCollection("paramaters")
        family = self.mongo_server.getCollection("part").find_one()['family']    
        return [i for x in parameters.find() if x['family'] == family for i in x.get('features') if i not in detector_predictions]

    def check_kanban(self, defect_list, features_list):
        if bool(defect_list) or len(features_list) >= 1:
            is_accepted = "Rejected"
        else:
            is_accepted = "Accepted" 
        return is_accepted

    def list_to_dict(self, defects):
        return {defect: defects.count(defect) for defect in defects}

    def save_image(self, dest, img):
        ret = cv2.imwrite(dest, img)
        return ret

    def check_status(self, EC, defects, predictor_list, TC=None):
        status = None
        TC_count_mismatch = 0
        EC_count_mismatch = 0

        character_count = self.list_to_dict(defects)

        if TC is not None:
            TC_count_mismatch = TC - character_count.get('TC', 0)

        if EC is not None:
            EC_count_mismatch = EC - character_count.get('EC', 0)

        family = self.redis_server.get_json("family")

        for x in self.mongo_server.getCollection("parameters").find({"family":family}):
            pass

        features_list = self.get_feature_list(predictor_list)
        defect_list = self.get_defect_list(defects)
        defect_list_count = self.list_to_dict(defect_list)

        for item in ['EC', 'TC']:
            try:
                features_list.remove(item)
            except:
                pass

        if features_list:
            features_list_count = self.list_to_dict(features_list)
            defect_list_count.update(features_list_count)

        if TC_count_mismatch != 0 or EC_count_mismatch != 0:
            status = 'Rejected'
        else:
            status = 'Accepted'

        defect_list_ = set(defect_list)
        defect_list = self.get_defect_list(list(defect_list_))

        for item in ['EC', 'TC']:
            try:
                defect_list.remove(item)
            except:
                pass

        status = self.check_kanban(defect_list, features_list)

        defect_details = defect_list_count
        if EC_count_mismatch > 0:
            defect_details['Elastomer_character_missing'] = EC_count_mismatch
        if TC_count_mismatch > 0:
            defect_details['Thermoset_character_missing'] = TC_count_mismatch

        return status, defect_details

