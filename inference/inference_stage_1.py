from flask import Flask, request, jsonify
from comman_utils import *
import time
from detect_module import *
import glob
import bson
import cv2
import time
import datetime
import json
from numba import cuda

app = Flask(__name__)

detector = None

@app.route("/model", methods=["GET"])       
def model():
    global detector 
    cuda.select_device(0)
    cuda.close()
    torch.cuda.empty_cache()
    current_inspection_collection_loaded = mongo_server.getCollection('current_inspection_details')
    current_inspection_details_loaded  = current_inspection_collection_loaded.find_one()
    current_inspection_data = current_inspection_details_loaded.get('current_inspection_details')
    part_type_data = current_inspection_data.get('part_type')
    config_collection_model = mongo_server.getCollection('config')
    config_dict_model = config_collection_model.find_one({"part_type":part_type_data})
    opt_model = Config(config_dict_model)
    detector = Detector(weights=opt_model.weights_path, half=False, device='cuda:0', img_size=1280)
    return json.dumps({'status': f'Completed Model Loading {part_type_data}'})

    
@app.route("/inspect", methods=["POST"])       
def inspect():
    process_util = Process()
    current_inspection_collection = mongo_server.getCollection('current_inspection_details')
    current_inspection_details  = current_inspection_collection.find_one()
    current_inspection_id = current_inspection_details.get('current_inspection_id')
    current_inspection = current_inspection_details.get('current_inspection_details')
    part_type = current_inspection.get('part_type')
    part_id = current_inspection.get('part_id')
    work_order = current_inspection.get('work_order')
    username = current_inspection.get('username')
    save_inspection_collection = mongo_server.getCollection(str(current_inspection_id) + '_results')
    redis_server.set_json({'inspection_process':True})
    date_folder = datetime.datetime.now().strftime('%Y-%m-%d')
    current_time = datetime.datetime.now().strftime('%H_%M_%S')
    save_data_path = prediction_save_path + date_folder+'/' + current_time
    process_util.check_dirs(save_data_path)
    config_collection = mongo_server.getCollection('config')
    config_dict = config_collection.find_one({"part_type":part_type})
    opt = Config(config_dict)
    combined_prediction = []
    input_images = []
    inference_images = []
    st = time.time()
    content = request.json
    for path in glob.glob('/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/inference/input_images/*.png'):
        original_filename = os.path.basename(path)
        image = cv2.imread(path)
        input_image = image.copy()
        predicted_frame, predictions, cords = detector.detector_get_inference(image, img_size=1280, stride=32, model=detector.model, device='cuda:0', half=False, opt=opt)
        for predict in predictions:
            combined_prediction.append(predict)
        execution_time = time.time() - st 
        unique_id = bson.ObjectId()
        ifname = str(unique_id)+'_ip.png'
        pfname = str(unique_id)+'_pf.png'
        cv2.imwrite(data_drive_path+ifname,input_image)
        cv2.imwrite(data_drive_path+pfname,predicted_frame)
        input_images.append('http://192.168.32.217:3306/'+ifname)
        inference_images.append('http://192.168.32.217:3306/'+pfname)
        cv2.imwrite(save_data_path +'/'+ 'stage_one_' + original_filename,predicted_frame)
        torch.cuda.empty_cache()
    
    stage_one_details = {}
    stage_one_details['part_id'] = part_id
    stage_one_details['part'] = part_type
    stage_one_details['work_order'] = work_order
    stage_one_details['operator'] = username
    stage_one_details['inspection_stage_one'] = {}
    stage_one_details['inspection_stage_one']['input_images'] = input_images
    stage_one_details['inspection_stage_one']['inference_images'] = inference_images
    stage_one_details['inspection_stage_one']['stage_one_prediction'] = combined_prediction
    stage_one_details['save_inspection_image_path'] = save_data_path
    stage_one_details['inspection_stage_two'] = {}
    stage_one_details['complete_inspection_details'] = {}
    stage_one_details['created_at'] = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    stage_one_details['time_stamp'] = str(datetime.datetime.now().replace(microsecond=0))
    stage_one_details['is_flag'] = False
    stage_one_details['complete_inspection_details'] = {}
    redis_server.set_json({'save_data_path' : save_data_path})
    save_inspection_collection.insert_one(stage_one_details)        
    print(f"TIME TAKEN TO EXICUTE {execution_time}")
    return json.dumps({'status': 'Completed Stage One successfully'})

if __name__ == "__main__":
    redis_server = CacheHelper()
    mongo_server = MongoHelper()
    data_drive_path = '/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/datadrive/'
    prediction_save_path = '/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/prediction_images/'
    app.run(host='0.0.0.0',port=8001,debug=False) 
