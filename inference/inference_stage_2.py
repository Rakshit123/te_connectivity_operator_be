from flask import Flask, request, jsonify
from comman_utils import *
import time
from detect_module import *
import glob
import bson
import cv2
import time
import datetime
import json
from numba import cuda

app = Flask(__name__)

@app.route("/model", methods=["GET"])       
def model():
    global detector 
    cuda.select_device(0)
    cuda.close()
    torch.cuda.empty_cache()
    current_inspection_collection_loaded = mongo_server.getCollection('current_inspection_details')
    current_inspection_details_loaded  = current_inspection_collection_loaded.find_one()
    current_inspection_data = current_inspection_details_loaded.get('current_inspection_details')
    part_type_data = current_inspection_data.get('part_type')
    config_collection_model = mongo_server.getCollection('config')
    config_dict_model = config_collection_model.find_one({"part_type":part_type_data})
    opt_model = Config(config_dict_model)
    detector = Detector(weights=opt_model.weights_path, half=False, device='cuda:0', img_size=1280)
    return json.dumps({'status': f'Completed Model Loading {part_type_data}'})

@app.route("/inspect", methods=["POST"])       
def inspect(): 
    current_inspection_collection = mongo_server.getCollection('current_inspection_details')
    current_inspection_details  = current_inspection_collection.find_one()
    current_inspection_id = current_inspection_details.get('current_inspection_id')
    current_inspection = current_inspection_details.get('current_inspection_details')
    part_type = current_inspection.get('part_type')
    save_inspection_collection = mongo_server.getCollection(str(current_inspection_id) + '_results')
    save_inspection_collection_details = save_inspection_collection.find_one(sort=[('_id', -1)])
    save_data_path = save_inspection_collection_details.get('save_inspection_image_path')
    config_collection = mongo_server.getCollection('config')
    config_dict = config_collection.find_one({"part_type":part_type})
    opt = Config(config_dict)
    combined_prediction = []
    input_images = []
    inference_images = []
    st = time.time()
    content = request.json
    for path in glob.glob('/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/inference/input_images/*.png'):
        original_filename = os.path.basename(path)
        image = cv2.imread(path)
        input_image = image.copy()
        predicted_frame, predictions, cords = detector.detector_get_inference(image, img_size=1280, stride=32, model=detector.model, device='cuda:0', half=False, opt=opt)
        for predict in predictions:
            combined_prediction.append(predict)
        execution_time = time.time() - st 
        unique_id = bson.ObjectId()
        ifname = str(unique_id)+'_ip.png'
        pfname = str(unique_id)+'_pf.png'
        cv2.imwrite(data_drive_path+ifname,input_image)
        cv2.imwrite(data_drive_path+pfname,predicted_frame)
        input_images.append('http://192.168.32.217:3306/'+ifname)
        inference_images.append('http://192.168.32.217:3306/'+pfname)
        cv2.imwrite(save_data_path +'/'+ 'stage_two_' + original_filename,predicted_frame)
        torch.cuda.empty_cache()
    
    stage_two_details = {}
    stage_two_details['inspection_stage_two'] = {}
    stage_two_details['inspection_stage_two']['input_images'] = input_images
    stage_two_details['inspection_stage_two']['inference_images'] = inference_images
    stage_two_details['inspection_stage_two']['stage_two_prediction'] = combined_prediction
    save_inspection_collection.update_one({"_id": save_inspection_collection_details['_id']}, {"$set": stage_two_details})
    redis_server.set_json({'save_inspection_id' : save_inspection_collection_details['_id']})
    
    current_inspection_details['inspection_stage_id'] = save_inspection_collection_details['_id']
    current_inspection_collection.update_one({"_id": current_inspection_details['_id']}, {"$set": current_inspection_details})
    print(f"TIME TAKEN TO EXICUTE {execution_time}")
    redis_server.set_json({'inspection_process':False})
    return json.dumps({'status': 'Completed Stage Two successfully'})

if __name__ == "__main__":
    redis_server = CacheHelper()
    mongo_server = MongoHelper()
    current_inspection_collection_loaded = mongo_server.getCollection('current_inspection_details')
    data_drive_path = '/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/datadrive/'
    prediction_save_path = '/home/lincode/TE_CONNECTIVITY_BACKEND/te_connectivity_operator_be/prediction_images/'
    app.run(host='0.0.0.0',port=8002,debug=False) 
