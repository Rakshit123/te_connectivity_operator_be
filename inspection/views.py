from json import encoder
import json
from django.http import HttpResponse, response
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import TemplateHTMLRenderer,JSONRenderer
from rest_framework.decorators import api_view, permission_classes
from common.utils import Encoder
from .utils import *
from rest_framework.permissions import AllowAny
from accounts.views import check_permission
from django.http import HttpResponse, StreamingHttpResponse ,JsonResponse
from bson import ObjectId
import os

@api_view(['GET'])
@csrf_exempt
@permission_classes((AllowAny,))
def getDefectList(request, inspectionid):
    from inspection.utils import get_defect_list
    response,status_code = get_defect_list(inspectionid)
    if status_code != 200:
        return HttpResponse( {response}, status=status_code)
    else:
        return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json") 

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def save_inspection_details_view(request):
    data = request.data
    from inspection.utils import save_inspection_details_util
    response,status_code = save_inspection_details_util(data)
    if status_code != 200:
        return HttpResponse( {response}, status=status_code)
    else:
        return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json") 

@api_view(['POST'])
@csrf_exempt
@permission_classes((AllowAny,))
def save_inspection_per_view(request):
    data = request.data
    from inspection.utils import save_inspection_per_view
    url = save_inspection_per_view(data)
    return HttpResponse(json.dumps({'data': url}), content_type="application/json")

@permission_classes((AllowAny,))
@api_view(['GET'])
@csrf_exempt
def get_data_stream(request, cameraid):
    from inspection.utils import Process
    check_permission(request,"can_get_capture_feed_url")
    process = Process()
    url = process.get_data_feed(cameraid)
    return HttpResponse(json.dumps({'data': url}), content_type="application/json")

# @api_view(['GET'])
# @renderer_classes((TemplateHTMLRenderer, JSONRenderer))
# @csrf_exempt
# @permission_classes((AllowAny,))
def get_metrics_view(request):
    from inspection.utils import GetMetrics
    def generate_stream():
        previous_doc = None
        while True:
            doc = GetMetrics()
            if doc.metrics != previous_doc:
                previous_doc = doc.metrics
                print(previous_doc)
                def json_default(obj):
                    if isinstance(obj, ObjectId):
                        return str(obj)
                    raise TypeError(f"Object of type {obj.__class__.__name__} is not JSON serializable")
                data_json = json.dumps(doc.metrics, default=json_default)
                yield f"data: {data_json}\n\n"
    return StreamingHttpResponse(generate_stream(), content_type="text/event-stream")

def get_health_view(request):
    from inspection.utils import HealthCheck
    def generate_health_status():
        previous_health_status = None
        while True:
            health = HealthCheck()
            if health.health_status != previous_health_status:
                previous_health_status = health.health_status
                print(previous_health_status)
                def json_default(obj):
                    if isinstance(obj, ObjectId):
                        return str(obj)
                    raise TypeError(f"Object of type {obj.__class__.__name__} is not JSON serializable")
                data_json = json.dumps(health.health_status, default=json_default)
                yield f"data: {data_json}\n\n"
    return StreamingHttpResponse(generate_health_status(), content_type="text/event-stream")

@api_view(['GET'])
@csrf_exempt
def get_camera_urls(request):
    url_list = [
        "http://127.0.0.1:8000//livis/v1/inspection/get_output_stream/input_frame/",
            CacheHelper().get_json("inference_frame"),
    ]
    print(url_list)
    return HttpResponse(json.dumps({'data': url_list}), content_type="application/json")


@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
def get_running_process_views(request):
    from inspection.utils import Process
    process = Process()
    response, status_code = process.get_running_process_util()
    if status_code != 200:
        return HttpResponse({response}, status = status_code)
    else:
        return HttpResponse(json.dumps(response, cls=Encoder), content_type = "application/json")

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def start_process(request):
    from inspection.utils import Process
    data = json.loads(request.body)
    process = Process()
    response,status_code = process.start_process_util(data)
    if status_code != 200:
        return HttpResponse( {response}, status=status_code)
    else:  
        return HttpResponse(json.dumps(response, cls=Encoder), content_type = "application/json")

@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def end_process(request):
    from inspection.utils import Process
    # data = json.loads(request.body)
    process = Process()
    response,status_code = process.end_process_util()
    if status_code != 200:
        return HttpResponse( {response}, status=status_code)
    else:
        return HttpResponse(json.dumps(response, cls=Encoder), content_type = "application/json")

@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer,))
@csrf_exempt
@permission_classes((AllowAny,))
def get_output_stream(request,cameraid):
    from inspection.utils import get_inference_feed
    return StreamingHttpResponse(get_inference_feed(cameraid), content_type='multipart/x-mixed-replace; boundary=frame')

@api_view(['GET'])
@csrf_exempt
@permission_classes((AllowAny,))
def getRedZoneList(request):
    data = CacheHelper().get_json('redzone_list')
    print(data)
    return HttpResponse(json.dumps({'data': data}), content_type="application/json")

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def start_inspection_details_view(request):
    data = json.loads(request.body)
    process = Process()
    response,status_code = process.start_inspection_details_util(data)
    if status_code != 200:
        return HttpResponse( {response}, status=status_code)
    else:
        return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")

@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer,JSONRenderer))
@csrf_exempt
@permission_classes((AllowAny,))
def flag_view(request):
    from inspection.utils import Process
    data = json.loads(request.body)
    process = Process()
    response,status_code = process.flag_process_util(data)
    if status_code != 200:
        return HttpResponse( {response}, status=status_code)
    else:
        return HttpResponse(json.dumps(response, cls=Encoder), content_type="application/json")        

