from django.contrib import admin
from django.urls import path, re_path
from django.urls import path,re_path
from django.conf.urls import url
from inspection import views

urlpatterns = [
    re_path(r'^get_capture_feed_url/$', views.get_camera_urls),
    re_path(r'^save_results_per_view/$', views.save_inspection_per_view),
    re_path(r'^get_running_process/$', views.get_running_process_views),
    re_path(r'^start_process/$', views.start_process),
    re_path(r'^end_process/$', views.end_process),
    ###################
    re_path(r'^start_inspection/$', views.start_inspection_details_view),
    re_path(r'^get_metrics/$', views.get_metrics_view),
    re_path(r'^set_flag/$', views.flag_view),    
    re_path(r'^get_health/$', views.get_health_view),    

]

