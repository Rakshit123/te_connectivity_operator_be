from common.utils import CacheHelper
from django.http import response
import inspection
import numpy as np
import cv2 
from numpy import append, array
import json
import base64
import multiprocessing
import sys
from pymongo import MongoClient
from bson import ObjectId
from livis import settings as settings
from livis.settings import BASE_URL
import os
import time
import datetime
from common.utils import *
import os
import shutil
import time
import datetime
from bson.objectid import ObjectId
import random
import requests
import concurrent.futures

class Process:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        self.datetime_now = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")  
    
    def del_keys(self,coll,keys_to_delete):
        for key in keys_to_delete:
            coll.pop(key, None)
        return coll
    
    def set_as_none(self,coll):
        for key in coll:
            coll[key] = None
        return coll  
      
    def load_model(self): 
        try:
            urls = ['http://127.0.0.1:8001/model', 'http://127.0.0.1:8002/model']
            def send_data(url):
                response = requests.get(url)
                return response.json()
            with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
                futures = [executor.submit(send_data, url) for url in urls]
                results = [future.result() for future in concurrent.futures.as_completed(futures)]
            print(results)
            return results
        except:
            print('ERROR in BACKEND FLASK SERVER')
            return results

                     
    def start_process_util(self, data):
        responce = {}
        variables = ['username','part_id','work_order','part','part_type','color','size','ec','tc']
        coll = {var: data.get(var, None) for var in variables}
        mp = self.mongo_server.getCollection('inspection')
        coll['start_time'] = self.datetime_now
        coll['produced_on'] = self.datetime_now
        coll['status'] = "started"
        coll['end_time'] = ""
        curr_insp_id = mp.insert_one(coll)
        
        current_inspection_details = self.mongo_server.getCollection('current_inspection_details')
        cd = current_inspection_details.find_one() 
        coll = self.del_keys(coll,['start_time','produced_on','status','end_time'])
        if cd is None:
            current_inspection_details.insert_one({'current_inspection_id': str(curr_insp_id.inserted_id) ,
                                                    'current_inspection_details':coll,
                                                    'time_stamp':self.datetime_now})
        else:
            current_inspection_details.update_one({"_id": cd['_id']}, {"$set": {'current_inspection_id': str(curr_insp_id.inserted_id) ,'current_inspection_details':coll,'time_stamp':self.datetime_now}})

        self.load_model()
        response = {"current_inspection_id": curr_insp_id.inserted_id}
        return response, 200
    
    def get_data_feed(self, topic):
        b = self.redis_server.get_json(topic)
        result = b
        return result

    def get_single_frame(self, topic):
        frame = self.redis_server.get_json(topic)
        return frame

    def create_urls_from_camera(self, camera_id, BASE_URL):
        fmt_url = BASE_URL + '/livis/v1/inspection/get_output_stream/{}/'
        return fmt_url.format(camera_id)

    def get_running_process_util(self):
        mp = self.mongo_helper.getCollection('inspection')
        last_started_doc = mp.find_one({"status": "started"}, sort=[('_id', -1)])
        response = {}
        if last_started_doc:
            response["inspection_id"] = str(last_started_doc['_id'])
            response["part_name"] = last_started_doc.get("part_name", None)
        return response, 200
    
    def end_process_util(self):
        current_inspection_details = self.mongo_server.getCollection('current_inspection_details')
        current_inspection_doc = current_inspection_details.find_one()
        inspection_details = self.mongo_server.getCollection('inspection')
        inspection_details_doc = inspection_details.find_one({'_id': ObjectId(current_inspection_doc["current_inspection_id"])})
        inspection_details.update_one({'_id': ObjectId(inspection_details_doc['_id'])}, {'$set': { "status": "completed" , "end_time": self.datetime_now}})
        if current_inspection_doc is None:
            current_inspection_details.insert_one({'current_inspection_id': None , 'current_inspection_details':None , 'time_stamp' :None})
        else:
            current_inspection_doc['current_inspection_id'] = None
            current_inspection_doc['current_inspection_details'] = None
            current_inspection_doc['time_stamp'] = None
            current_inspection_doc['inspection_stage_id'] = None
            current_inspection_details.update_one({"_id": current_inspection_doc['_id']}, {"$set": current_inspection_doc})
        return {}, 200
    
    def flag_process_util(self, data):
        try:
            variables = ['message', 'is_flag', 'check_box_details']
            coll = {var: data.get(var, None) for var in variables}
            c_id = self.mongo_server.getCollection('current_inspection_details')
            doc = c_id.find_one()
            mp = self.mongo_server.getCollection(str(doc['current_inspection_id']) + "_results")
            latest_doc = mp.find_one({}, sort=[('_id', -1)])
            if latest_doc is None:
                mp.insert_one(coll)
            else:
                mp.update_one({'_id': latest_doc['_id']}, {"$set": coll})
            # _id_new = latest_doc.get('_id') if latest_doc else None
            # image = CacheHelper().get_json('flag_input_frame')
            # if _id_new:
            #     cv2.imwrite(f"save_flag_image/save_flag_image_{_id_new}.jpg", image)
            return "success", 200
        except Exception as e:
            print(e)
            return str(e), 400
        
    def is_rejected(self,EC,TC):
        mismatch_list = []
        status = 'Accepted'
        ec_mismatch = None
        tc_mismatch = None
        current_inspection_details = self.mongo_server.getCollection('current_inspection_details')
        current_inspection_doc = current_inspection_details.find_one()
        current_inspection_details = current_inspection_doc['current_inspection_details']
        EC_count = current_inspection_details["ec"]
        TC_count = current_inspection_details["tc"]
        print(EC_count,TC_count ,'EC_COUNT')
        if EC == int(EC_count) :
            ec_mismatch = False
        else:
            ec_mismatch = True
            mismatch_list.append('Elestromer Character Missing')
        if TC is None or TC == int(TC_count):
            tc_mismatch = False
        else:
            tc_mismatch = True    
            mismatch_list.append('Thermoset Character Missing')
        if ec_mismatch == True or tc_mismatch == True:
            status = 'Rejected'
        return status , mismatch_list

    def start_inspection_details_util(self , data):
        try:
            url1 = "http://localhost:8001/inspect"
            url2 = "http://localhost:8002/inspect"
            data1 = {"key1": "value1"}
            data2 = {"key2": "value2"}
            response1 = requests.post(url1, json=data1)
            if response1.status_code == 200:
                print("First POST request successful")
                response2 = requests.post(url2, json=data2)
                if response2.status_code == 200:
                    current_inspection_details = self.mongo_server.getCollection('current_inspection_details')
                    current_inspection_doc = current_inspection_details.find_one()
                    current_inspection_id = current_inspection_doc["current_inspection_id"]
                    save_inspection_collection = self.mongo_server.getCollection(str(current_inspection_id) + '_results')
                    save_inspection_collection_details = save_inspection_collection.find_one(sort=[('_id', -1)])
                    print("Second POST request successful")
                    status , mismatch_list = self.is_rejected(10,8)
                    complete_inspection_details = {}
                    complete_inspection_details['complete_inspection_details'] = {'EC' : 10 , 'TC' : 8 , 'Defects' : [] , 'Features_Missing' : mismatch_list , 'status' : status}
                    save_inspection_collection.update_one({"_id": save_inspection_collection_details['_id']}, {"$set": complete_inspection_details})
                else:
                    print(f"Second POST request failed with status code: {response2.status_code}")
            else:
                print(f"First POST request failed with status code: {response1.status_code}")
            return 'Succesfully Stage one and Stage Two inspection is completed' , 200   
        except Exception as e:
            return str(e) , 500   
            
class GetMetrics:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        self.current_inspection_collection = self.mongo_server.getCollection('current_inspection_details')
        self.current_inspection_details  = self.current_inspection_collection.find_one()
        self.metrics = self.inspection_metrics()
    
    def inspection_metrics(self):
        try:
            is_process_started = None
            complete_inspection_details = None
            inspection_stage_1 = None
            inspection_stage_2 = None
            inspection_id = self.current_inspection_details['current_inspection_id']
            current_inspection_details = self.current_inspection_details['current_inspection_details']
            mp = self.mongo_server.getCollection(str(inspection_id) + '_results')
            dataset = [p for p in mp.find().sort("$natural", -1)]
            dataset1 = [p for p in mp.find({"complete_inspection_details.status": "Accepted" })]
            dataset2 = [p for p in mp.find({"complete_inspection_details.status": "Rejected" })]
            total_production = len(dataset)
            total_accepted_count = len(dataset1)
            total_rejected_count = len(dataset2)
            inspection_stage_id = self.current_inspection_details['inspection_stage_id']
            inspection_doc = mp.find_one({'_id':ObjectId(inspection_stage_id)})
            inspection_metrics_obj = {}
            inspection_details = self.current_inspection_details['current_inspection_details']
            if self.current_inspection_details['current_inspection_id'] is None :
                is_process_started = False
            else:
                is_process_started = True   
            if inspection_doc:
                try:
                    if len(inspection_doc['inspection_stage_two']) != 0 : 
                        inspection_stage_1 = inspection_doc['inspection_stage_two']
                    if len(inspection_doc['inspection_stage_two']) != 0 :
                        inspection_stage_2 = inspection_doc['inspection_stage_two']  
                    if len(inspection_doc['complete_inspection_details']) != 0 :
                        complete_inspection_details = inspection_doc['complete_inspection_details']        
                except:
                    inspection_stage_1 = None 
                    inspection_stage_2 = None  
                    complete_inspection_details = None        
            if inspection_stage_1 is not None and inspection_stage_2 is not None:
                inspection_metrics_obj['inspection_details'] = inspection_details
                inspection_metrics_obj['inspection_stage_one'] = inspection_stage_1
                inspection_metrics_obj['inspection_stage_two'] = inspection_stage_2
                inspection_metrics_obj['complete_inspection_details'] = complete_inspection_details
                inspection_metrics_obj['total'] = str(total_production)
                inspection_metrics_obj['total_accepted'] = str(total_accepted_count)
                inspection_metrics_obj['total_rejected'] = str(total_rejected_count)
                inspection_metrics_obj['is_processing'] = self.redis_server.get_json("inspection_process")
                inspection_metrics_obj['is_running'] = is_process_started
            else:
                inspection_metrics_obj['inspection_details'] = inspection_details
                inspection_metrics_obj['is_running'] = is_process_started  
            inspection_metrics = {
                "inspection_metrics":inspection_metrics_obj,
            }
            return inspection_metrics
        except Exception as e:
            print(e)
            inspection_metrics_obj = {}
            is_process_started =  None
            if self.current_inspection_details['current_inspection_id'] is None :
                is_process_started = False
            else:
                is_process_started = True  
            inspection_metrics_obj['is_running'] = is_process_started
            inspection_metrics = {"inspection_metrics":inspection_metrics_obj}
            return inspection_metrics

    def get_inference_feed(self, cam_id):
        print(cam_id)
        key = str(cam_id)
        while True:
            v = self.redis_server.get_json(key)
            im_b64_str = v
            ret, jpeg = cv2.imencode('.jpg', im_b64_str)
            frame = jpeg.tobytes()

            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
            
class HealthCheck:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        self.health_status = self.health_check()          
    
    def health_check(self):
        try:
            health_check_obj = {}
            health_check_obj['software_health_details'] = {}
            health_check_obj['software_health_details']['AI INSPECTION SERVER ONE'] = True
            health_check_obj['software_health_details']['AI INSPECTION SERVER TWO'] = True
            health_check_obj['software_health_details']['BACKEND SERVER'] = True
            health_check_obj['software_health_details']['PLC SERVER'] = True
            health_check_obj['software_health_details']['DATA BASE SERVER'] = True
            health_check_obj['software_health_details']['TEMP MEMORY SERVER'] = True
            health_check_obj['hardware_health_details'] = {}
            health_check_obj['hardware_health_details']['PLC STATUS'] = True
            health_check_obj['hardware_health_details']['CAMERA STATUS'] = True
            health_check_obj['hardware_health_details']['LIGHT STATUS'] = True
            health_check_obj['hardware_health_details']['SENSOR STATUS'] = True
            health_metrics = {
                "health_metrics":health_check_obj,
            }
            return health_metrics
        except Exception as e:
            print(e)
            health_check_obj = {}
            health_metrics = {
                "health_metrics":health_check_obj,
            }
            return health_metrics    
        
        


                
        
    


